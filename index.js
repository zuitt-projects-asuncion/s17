/*
	ACTIVITY:

	>> Create an addStudent() function that will accept a name of the student and add it to the student array.

	>> Create a countStudents() function that will print the total number of students in the array.

	>> Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

	STRETCH GOALS:

	>> Create a findStudent() function that will do the following:
		--Search for a student name when a keyword is given (filter method).
		--If one match is found print the message studentName is an enrollee.
		--If multiple matches are found print the message studentNames are enrollees.
		--If no match is found print the message studentName is not an enrollee.
		--The keyword given should not be case sensitive.

*/
let students = ["Barabas", "John", "Luke", "Krull", "Mark"];
let student = ("Matthew");

function addStudent(student){
		students.unshift(student);

		};//END if addStudent


function countStudents(students){
		console.log("There are currently " + students.length + " students.");

		};//END countStudents

function printStudents(students){
		students.sort();
};//End printStudents

addStudent(student);
countStudents(students);
printStudents(students);
console.log(student + " was added to the student's list.");
console.log("Here they are in alphabetical order: " + students);